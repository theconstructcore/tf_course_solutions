# Launch the Keyboard teleop for moving the turtle
roslaunch turtle_tf_3d turtle_keyboard_move.launch
# Launch the irobot listener to make irobot follow you
rosrun turtle_tf_3d turtle_tf_listener.py turtle2 turtle1
# Launch the multiple tf publishers that will publish turtle1, turtle2 and coke_can tfs needed
python tf_course_solutions_pkg multiple_broadcaster_ex2_1.py
# tviz to check tfs are working
rviz
#---> All this will be in this launch concentrated:
roslaunch tf_course_solutions_pkg ex_2_1_solution.launch

# Launch the different fixed and moving publishers
rosrun tf_course_solutions_pkg fixed_extra_frame_pub_ex2_4.py
rosrun tf_course_solutions_pkg moving_extra_frame_pub_ex2_4.py

