# Launch the Keyboard teleop for moving the turtle
roslaunch turtle_tf_3d turtle_keyboard_move.launch
# Launch the irobot listener to make irobot follow you
rosrun turtle_tf_3d turtle_tf_listener.py turtle2 turtle1
# Launch the multiple tf publishers that will publish turtle1, turtle2 and coke_can tfs needed
python ex_2_1 multiple_broadcaster_ex2_1.py
# tviz to check tfs are working
rviz
